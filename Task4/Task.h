#pragma once
#define _USE_MATH_DEFINES
#define DBL_MAX 1.7976931348623158e+308
#define MAX_ITERS 1000000

#include<iostream>
#include<limits>
#include<cstdlib>
#include<mpi.h>


class Task
{
	int ProcCount, ProcRank;
    double Xm[2],Ym[2],Zm[2];
	int Im,Jm,Km;
    int localK;
    double a;
    double hx,hy,hz;
    double epsilon;
	double* oldA;
    double* newA;
    double* oldNext[2];
//    double* newNext[2];

    int minK;

    double p(double x, double y, double z);
    double fi(double x, double y, double z);
    double F(double x, double y, double z);
    double MaxDifference();
    void printMatrix();

	//void set();
public:
	void run();
    Task (double XmMin,double XmMax,double YmMin,double YmMax,double ZmMin, double ZmMax,int Im,int Jm,int Km,
          double a, double epsilon);
	~Task();
};