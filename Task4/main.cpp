#include <cstdio>
#include "Task.h"
#include <cmath>

int main (int argc, char **argv)
{
//    if (argc == 2)
//    {
        //Инициализируем библиотеку
        MPI_Init (&argc, &argv);

        Task t (-1, 1, -1, 1, -1, 1, 50,50,50, 0.001, 0.0001);
        t.run ();

//        int rank, size;
//        MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
//        MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */
//        printf( "Hello world from process %d of %d\n", rank, size );

        //Все задачи завершают выполнение
        MPI_Finalize ();
    //}
    //else
   //     std::cout << "use 1 argument file name" << std::endl;
    return 0;
}
