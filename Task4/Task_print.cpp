#include <iomanip>
#include "Task.h"
using namespace std;
void Task::printMatrix ()
{
    cout << "matrix in proc number: " << ProcRank << "\n";
    for (int k = 0; k < localK; k++)
    {
        for (int j = 0; j < Jm; j++)
        {
            for (int i = 0; i < Im; i++)
            {
                cout << setw (6) << fixed << setprecision (2) << newA[k * Im * Jm + j * Im + i] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
    //cout << "max dif: "<< fixed << setprecision (10) << MaxDifference ()<< endl;
}
