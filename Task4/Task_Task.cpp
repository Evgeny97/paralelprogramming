#include <fstream>
#include "Task.h"

using namespace std;

Task::Task (double XmMin, double XmMax, double YmMin, double YmMax, double ZmMin, double ZmMax, int Im, int Jm, int Km, double a, double epsilon)
{
    MPI_Comm_size (MPI_COMM_WORLD, &ProcCount);
    MPI_Comm_rank (MPI_COMM_WORLD, &ProcRank);
    Xm[0] = XmMin;
    Xm[1] = XmMax;
    Ym[0] = YmMin;
    Ym[1] = YmMax;
    Zm[0] = ZmMin;
    Zm[1] = ZmMax;
    this->Im = Im;
    this->Jm = Jm;
    this->Km = Km;
    this->a = a;
    this->epsilon = epsilon;
    localK = (Km / ProcCount) + ((ProcRank < Km % ProcCount) ? 1 : 0);
    hx = (Xm[1] - Xm[0]) / (Im - 1);
    hy = (Ym[1] - Ym[0]) / (Jm - 1);
    hz = (Zm[1] - Zm[0]) / (Km - 1);
//    cout<<hz<<endl;
    if (ProcRank < Km % ProcCount)
    {
        minK = localK * ProcRank;
    }
    else
    {
        minK = localK * ProcRank + (Km % ProcCount);
    }

    oldA = new double[Im * Jm * localK];
    newA = new double[Im * Jm * localK];

    if (ProcRank != 0)
    {
        oldNext[0] = new double[Im * Jm];
        for (int i = 0; i < Jm * Im; i++)
            oldNext[0][i] = 0;

    }

    if (ProcRank != ProcCount - 1)
    {
        oldNext[1] = new double[Im * Jm];
        for (int i = 0; i < Jm * Im; i++)
            oldNext[1][i] = 0;
    }

    for (int k = 0; k < localK; k++)
        for (int j = 1; j < Jm - 1; j++)
            for (int i = 1; i < Im - 1; i++)
                newA[k * Im * Jm + j * Im + i] = 0;

    for (int k = 0; k < localK; k++)
    {
        for (int i = 0; i < Im; ++i)
        {
            newA[k * Im * Jm + 0 * Im + i] = fi (Xm[0] + i * hx, Ym[0] + 0 * hy, Zm[0] + (minK + k) * hz);
            newA[k * Im * Jm + (Jm - 1) * Im + i] = fi (Xm[0] + i * hx, Ym[0] + (Jm - 1) * hy, Zm[0] + (minK + k) * hz);
        }
        for (int j = 1; j < Jm - 1; ++j)
        {
            newA[k * Im * Jm + j * Im + 0] = fi (Xm[0] + 0 * hx, Ym[0] + j * hy, Zm[0] + (minK + k) * hz);
            newA[k * Im * Jm + j * Im + (Im - 1)] = fi (Xm[0] + (Im - 1) * hx, Ym[0] + j * hy, Zm[0] + (minK + k) * hz);
        }
    }
    if (ProcRank == 0)
        for (int i = 1; i < Im - 1; ++i)
            for (int j = 1; j < Jm - 1; ++j)
                newA[0 * Im * Jm + j * Im + i] = fi (Xm[0] + i * hx, Ym[0] + j * hy, Zm[0] + 0 * hz);
    if (ProcRank == ProcCount - 1)
        for (int i = 1; i < Im - 1; ++i)
            for (int j = 1; j < Jm - 1; ++j)
                newA[(localK - 1) * Im * Jm + j * Im + i] =
                    fi (Xm[0] + i * hx, Ym[0] + j * hy, Zm[0] + (minK + localK - 1) * hz);

    for (int i = 0; i < localK * Jm * Im; i++)
        oldA[i] = newA[i];

//    for (int l = 0; l < ProcCount; ++l)
//    {
//        if (ProcRank == l)
//            printMatrix ();
//        MPI_Barrier (MPI_COMM_WORLD);
//    }
}

Task::~Task ()
{
    delete[] oldA;
    delete[] newA;
}