#include <iomanip>
#include"Task.h"

using namespace std;

void Task::run ()
{
    double max_dif = 9999;
    double startTime = 0;
    if (ProcRank == 0)
        startTime = MPI_Wtime ();

    double denominator = (2 / (hx * hx) + 2 / (hy * hy) + 2 / (hz * hz) + a);
    int uslovka = 1000000;
    MPI_Request request1, request2, request3, request4;
    MPI_Status status;
    while (uslovka)
    {
        uslovka--;
        if (ProcRank != 0)
        {
            if (!(ProcRank == ProcCount - 1 && localK == 1))
            {
                for (int j = 1; j < Jm - 1; ++j)
                {
                    for (int i = 1; i < Im - 1; ++i)
                    {
                        newA[0 + j * Jm + i] = ((oldA[j * Jm + i + 1] + oldA[j * Jm + i - 1]) / (hx * hx)
                            + (oldA[(j + 1) * Jm + i] + oldA[(j - 1) * Jm + i]) / (hy * hy)
                            + (((localK > 1) ? oldA[1 * Jm * Im + j * Jm + i] : oldNext[1][j * Jm + i])
                                + oldNext[0][j * Jm + i]) / (hz * hz)
                            - p (Xm[0] + i * hx, Ym[0] + j * hy, Zm[0] + (minK + 0) * hz)) / denominator;
                    }
                }
            }
        }
        if (ProcRank != ProcCount - 1)
        {
            if (!(ProcRank == 0 && localK == 1))
            {
                for (int j = 1; j < Jm - 1; ++j)
                {
                    for (int i = 1; i < Im - 1; ++i)
                    {
                        newA[(localK - 1) * Jm * Im + j * Jm + i] = ((oldA[(localK - 1) * Jm * Im + j * Jm + i + 1]
                            + oldA[(localK - 1) * Jm * Im + j * Jm + i - 1]) / (hx * hx)
                            + (oldA[(localK - 1) * Jm * Im + (j + 1) * Jm + i]
                                + oldA[(localK - 1) * Jm * Im + (j - 1) * Jm + i]) / (hy * hy)
                            + (oldNext[1][j * Jm + i]
                                + ((localK > 1) ? oldA[(localK - 2) * Jm * Im + j * Jm + i] : oldNext[0][j * Jm + i]))
                                / (hz * hz)
                            - p (Xm[0] + i * hx, Ym[0] + j * hy, Zm[0] + (minK + localK - 1) * hz)) / denominator;
                    }
                }
            }
        }

        //        if (ProcRank != 0)
        //        {
        //            MPI_Send (&newA[0], Jm * Im, MPI_DOUBLE, ProcRank - 1, 123, MPI_COMM_WORLD);
        //        }
        //        if (ProcRank != ProcCount - 1)
        //        {
        //            MPI_Recv (oldNext[1], Jm * Im, MPI_DOUBLE, ProcRank + 1, 123, MPI_COMM_WORLD, &status);
        //            MPI_Send (&newA[(localK - 1) * Jm * Im], Jm * Im, MPI_DOUBLE, ProcRank + 1, 123, MPI_COMM_WORLD);
        //        }
        //        if (ProcRank != 0)
        //        {
        //            MPI_Recv (oldNext[0], Jm * Im, MPI_DOUBLE, ProcRank - 1, 123, MPI_COMM_WORLD, &status);
        //        }

        if (ProcRank != 0)
        {
            MPI_Isend (&newA[0], Jm * Im, MPI_DOUBLE, ProcRank - 1, 1, MPI_COMM_WORLD, &request1);
            MPI_Irecv (oldNext[0], Jm * Im, MPI_DOUBLE, ProcRank - 1, 2, MPI_COMM_WORLD, &request2);
        }
        if (ProcRank != ProcCount - 1)
        {
            MPI_Isend (&newA[(localK - 1) * Jm * Im], Jm * Im, MPI_DOUBLE, ProcRank + 1, 2, MPI_COMM_WORLD, &request3);
            MPI_Irecv (oldNext[1], Jm * Im, MPI_DOUBLE, ProcRank + 1, 1, MPI_COMM_WORLD, &request4);
        }

        for (int k = 1; k < localK - 1; ++k)
        {
            for (int j = 1; j < Jm - 1; ++j)
            {
                for (int i = 1; i < Im - 1; ++i)
                {
                    newA[k * Jm * Im + j * Jm + i] = ((oldA[k * Jm * Im + j * Jm + i + 1]
                        + oldA[k * Jm * Im + j * Jm + i - 1]) / (hx * hx)
                        + (oldA[k * Jm * Im + (j + 1) * Jm + i] + oldA[k * Jm * Im + (j - 1) * Jm + i]) / (hy * hy)
                        + (oldA[(k + 1) * Jm * Im + j * Jm + i] + oldA[(k - 1) * Jm * Im + j * Jm + i]) / (hz * hz)
                        - p (Xm[0] + i * hx, Ym[0] + j * hy, Zm[0] + (minK + localK - 1) * hz)) / denominator;
                }
            }
        }

        //        for (int l = 0; l < ProcCount; ++l)
        //        {
        //            if (ProcRank == l)
        //                printMatrix ();
        //            MPI_Barrier (MPI_COMM_WORLD);
        //        }

        swap<double *> (oldA, newA);
        max_dif = MaxDifference ();
        if (ProcRank == 0)
          cout << "max dif: " << fixed << setprecision (10) << max_dif << endl;
        if (max_dif < epsilon)
            break;

        if (ProcRank != 0)
        {
            MPI_Wait (&request1, &status);
            MPI_Wait (&request2, &status);
        }
        if (ProcRank != ProcCount - 1)
        {
            MPI_Wait (&request3, &status);
            MPI_Wait (&request4, &status);
        }
    }
    if (ProcRank == 0)
        cout << "max dif: " << fixed << setprecision (10) << max_dif << endl;
    if (ProcRank == 0)
        cout << "time:" << MPI_Wtime () - startTime;
}
	



