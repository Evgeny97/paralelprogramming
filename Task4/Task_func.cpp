//
// Created by Evgeny on 13.05.2017.
//

#include "Task.h"
#include <cmath>
using namespace std;

double Task::p (double x, double y, double z)
{
    return 6 - a * fi (x, y, z);
}

double Task::fi (double x, double y, double z)
{
    return x * x + y * y + z * z;
}
double Task::F (double x, double y, double z)
{
    return fi (x, y, z);
}
double Task::MaxDifference ()
{
    double max = 0;
    for (int k = 0; k < localK; k++)
        for (int j = 0; j < Jm; j++)
            for (int i = 0; i < Im; i++)
                if (abs (newA[k * Im * Jm + j * Im + i] - fi (Xm[0] + i * hx, Ym[0] + j * hy, Zm[0] + (minK + k) * hz)) > max)
                    max = abs (newA[k * Im * Jm + j * Im + i] - fi (Xm[0] + i * hx, Ym[0] + j * hy, Zm[0] + (minK + k) * hz));
    double nMax;
    MPI_Allreduce (&max,&nMax,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
    return nMax;
}
