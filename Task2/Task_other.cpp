#include"Task.h"
#include<cmath>
#include <cstdio>
#include <omp.h>

using namespace std;

void Task::printA() {
    for (int y = 0; y < N; y++) {
        for (int _x = 0; _x < N; _x++) {
            printf("%.2f ", A[_x + y * N]);
        }
        cout << endl;
    }
}

void Task::printX(double *x, int n/* = this.N*/) {
    for (int i = 0; i < n; i++) {
        printf("%.2f ", x[i]);
    }
    cout << endl;
}

void Task::printX(double *x) {
    printX(x, N);
}

double Task::measure(double *x) {
    double sum = 0;
    for (int i = 0; i < N; i++) {
        sum += x[i] * x[i];
    }
    return sqrt(sum);
}

double Task::measureOfSums(double x) {
    return sqrt(x);
}

void Task::multiplyAvec(double *vec, double *rez) {
//#pragma omp parallel for num_threads(16)
#pragma omp for schedule(static, 10)
    for (int y = 0; y < N; y++) {
        rez[y] = 0;
        for (int x = 0; x < N; x++) {
            rez[y] += A[x + y * N] * vec[x];
        }
        //cout << omp_get_thread_num();
    }
}

void Task::set(int way) {
    switch (way) {
        case 1:
            for (int _y = 0; _y < N; _y++) {
                for (int _x = 0; _x < N; _x++) {
                    if (_x == _y)
                        A[_x + _y * N] = 2.0f;
                    else
                        A[_x + _y * N] = 1.0f;
                }
                b[_y] = N + 1;
            }
            //printA();
            measureOfB = measure(b);
            for (int _y = 0; _y < N; _y++)
                X[_y] = 0;

            break;
        case 2: {
            double *tArr = new double[N];
            for (int _y = 0; _y < N; _y++) {
                for (int _x = 0; _x < N; _x++) {
                    if (_x == _y)
                        A[_x + _y * N] = 2.0f;
                    else
                        A[_x + _y * N] = 1.0f;
                }
                tArr[_y] = sin(2 * M_PI * _y / N);
            }
            //printX(tArr);
            multiplyAvec(tArr, b);
            //printA();
            measureOfB = measure(b);
            delete[] tArr;

            for (int _y = 0; _y < N; _y++)
                X[_y] = 0;
            break;
        }
        default:
            cout << "Bad way number";
            exit(1);
    }
}

Task::Task(int way, int n) {
    N = n;
    A = new double[N * N];
    b = new double[N];
    X = new double[N];
}

Task::~Task() {
    delete[] A;
    delete[] X;
    delete[] b;
}
