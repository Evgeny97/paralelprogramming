#include "Task.h"

int main(int argc, char **argv)
{
	if (argc == 3)
	{
		Task t(atoi(argv[1]), atoi(argv[2]));
		t.run(atoi(argv[1]));
	}
	else
	{
		std::cout << "use 2 arguments way and N" << std::endl;
	}

	return 0;
}


