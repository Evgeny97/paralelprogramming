#include"Task.h"
#include <omp.h>
#include <time.h>

using namespace std;

void Task::run(int way) {

    const double epsilon = 0.00001;
    double lastMeasure = DBL_MAX;
    double newMeasure;
    double *X1 = new double[N];

    double workTime = 0;
    double localSumSquads = 0;
    int iters = 0;
    set(way);
    for (int i = 0; i < N; i++)
        X1[i] = 0;
    workTime = omp_get_wtime();
    #pragma omp parallel num_threads(1)
    {
        while (lastMeasure > epsilon && iters < MAX_ITERS) {
            #pragma  omp single
            {
                iters++;
                localSumSquads = 0;
            }
            multiplyAvec(X, X1);
            //omp_set_num_threads(4);
            //cout << omp_get_num_procs();
            //cout << omp_get_thread_num();

            //#pragma omp parallel for num_threads(16) reduction(+:localSumSquads)
            #pragma omp for schedule(static, 10) reduction(+:localSumSquads)
            for (int i = 0; i < N; i++) {
                X1[i] -= b[i];
                localSumSquads += X1[i] * X1[i];
                X1[i] = X[i] - (tau * (X1[i]));
                //cout << omp_get_thread_num();
            }
            //printX(X1);
            #pragma  omp single
            {
                swap<double *>(X, X1);
                newMeasure = measureOfSums(localSumSquads) / measureOfB;
                //cout << newMeasure << endl;
                if (newMeasure > lastMeasure)
                    tau = -tau;
                lastMeasure = newMeasure;
            }
        }
    }
    if (iters != MAX_ITERS) {
        workTime = omp_get_wtime() - workTime;
        cout << "\nThe answer was found in " << iters << " iters\n";
        cout << "Measure of difference is: " << lastMeasure << "\n";
        cout << "Work time: " << (double) workTime << " sec\n";
    } else {
        cout << "Can't find answer in " << MAX_ITERS << " iters\n";
        cout << "Measure of difference is: " << lastMeasure << "\n";
    }

}



