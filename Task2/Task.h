#pragma once
#define _USE_MATH_DEFINES
#define DBL_MAX 1.7976931348623158e+308
#define MAX_ITERS 1000000

#include<iostream>
#include<limits>
#include<cstdlib>
#include"Task.h"

class Task
{
	int N;
	double measureOfB;
	double *A = NULL;
	double *b = NULL;
	double *X = NULL;	
	double tau = 0.00001;
	void printA();
	void printX(double* x, int n);
	void printX(double* x);
	double measure(double* x);
    double measureOfSums(double x);
	void multiplyAvec(double* vec, double* rez);
	void set(int way);
public:
	void run(int way);	
	Task(int way, int n);
	~Task();
};