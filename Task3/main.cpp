#include <cstdio>
#include "Task.h"

int main (int argc, char **argv)
{
    if (argc == 2)
    {
        //Инициализируем библиотеку
        MPI_Init (&argc, &argv);

        Task t (argv[1]);
        t.run ();

//        int rank, size;
//        MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
//        MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */
//        printf( "Hello world from process %d of %d\n", rank, size );

        //Все задачи завершают выполнение
        MPI_Finalize ();
    }
    else
        std::cout << "use 1 argument file name" << std::endl;
    return 0;
}
