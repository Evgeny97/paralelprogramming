#include "Task.h"
using namespace std;
void Task::printMatrix(double *m, int mx, int my) {
    cout<<"matrix:\n";
    for (int y = 0; y < my; y++) {
        for (int x = 0; x < mx; x++) {
            cout << m[y * mx + x]<<" ";
        }
        cout << endl;
    }
}

