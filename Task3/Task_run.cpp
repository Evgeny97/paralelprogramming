#include"Task.h"

using namespace std;

void Task::run ()
{
    double strtTime;
    if (ProcRank == 0)
        strtTime = MPI_Wtime ();
    MPI_Bcast (&AxLenght, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&AyLenght, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&BxLenght, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&ByLenght, 1, MPI_INT, 0, MPI_COMM_WORLD);

    int dim_size[2] = {0, 0};
    MPI_Dims_create (ProcCount, 2, dim_size);
    //cout << dim_size[0] << dim_size[1];
    int periods[2] = {0, 0};
    MPI_Cart_create (MPI_COMM_WORLD, 2, dim_size, periods, 1, &myDecComm);

    MPI_Cart_coords (myDecComm, ProcRank, 2, coords);
//    int r;
//    MPI_Cart_rank (myDecComm, coords,&r);
//    cout << r;
    int remaining[2] = {0, 1};
    MPI_Cart_sub (myDecComm, remaining, &rowComm);
    remaining[0] = 1;
    remaining[1] = 0;
    MPI_Cart_sub (myDecComm, remaining, &colComm);

    localYLenght = AyLenght / dim_size[0];
    localXLenght = BxLenght / dim_size[1];

//    cout <<"localXYLenght: "<< localXLenght << " " << localYLenght << endl;
//    cout <<"coords: "<< coords[0] << " " << coords[1] << endl;
//    cout <<"dimsize: "<< dim_size[0] << " " << dim_size[1] << endl;
    localA = new double[AxLenght * localYLenght];
    if (coords[1] == 0)
    {
        MPI_Scatter (A, AxLenght * localYLenght, MPI_DOUBLE, localA, AxLenght * localYLenght, MPI_DOUBLE, 0, colComm);

//        for (int i = 0; i < dim_size[0]; ++i) {
//            if (i == coords[0])
//                printMatrix(localA, AxLenght, localYLenght);
//            MPI_Barrier(colComm);
//        }
    }

    MPI_Datatype type1, type2;
    MPI_Type_vector (ByLenght, localXLenght, BxLenght, MPI_DOUBLE, &type1);
    MPI_Type_commit (&type1);
    MPI_Type_create_resized (type1, 0, localXLenght * sizeof (double), &type2);
    MPI_Type_commit (&type2);

    MPI_Barrier (MPI_COMM_WORLD);
    localB = new double[ByLenght * localXLenght];
    if (coords[0] == 0)
    {
        MPI_Scatter (B, /*dim_size[1]*/1, type2, localB, ByLenght * localXLenght, MPI_DOUBLE, 0, rowComm);
//        int r;
//        int z =1;
//        MPI_Cart_rank(colComm,&z , &r);
//        cout << r;

//        for (int i = 0; i < dim_size[1]; ++i) {
//            if (i == coords[1])
//                printMatrix(localB, localXLenght, ByLenght);
//            MPI_Barrier(rowComm);
//        }
    }
    MPI_Bcast (localA, AxLenght * localYLenght, MPI_DOUBLE, 0, rowComm);
    MPI_Bcast (localB, ByLenght * localXLenght, MPI_DOUBLE, 0, colComm);
    localC = new double[localYLenght * localXLenght];
    multiply ();
//    if (coords[0] == 0 && coords[1] == 0)
//    {
//        printMatrix (localA, AxLenght, localYLenght);
//        printMatrix (localB, localXLenght, ByLenght);
//        printMatrix (localC, localXLenght, localYLenght);
//    }
    MPI_Datatype type3, type4;
    MPI_Type_vector (localYLenght, localXLenght, BxLenght, MPI_DOUBLE, &type3);
    MPI_Type_commit (&type3);
    MPI_Type_create_resized (type3, 0, localXLenght * sizeof (double), &type4);
    MPI_Type_commit (&type4);

    int *receive_counts = new int[ProcCount];
    int *receive_displacements = new int[ProcCount];

    if (ProcRank == 0)
    {
        C = new double[BxLenght * AyLenght];
        for (int y = 0; y < AyLenght; y++)
            for (int x = 0; x < BxLenght; x++)
            {
                C[y * BxLenght + x] = 0;
            }
    }
    for (int i = 0; i < ProcCount; ++i)
    {
        receive_counts[i] = 1;
        receive_displacements[i] = (i / dim_size[1]) * dim_size[1] * localYLenght + (i % dim_size[1]);
    }
//    for (int y = 0; y < dim_size[0]; ++y)
//    {
//        for (int x = 0; x < dim_size[1]; ++x)
//        {
//            receive_counts[y * dim_size[1] + x] = 1;
//            receive_displacements[y * dim_size[1] + x] = y * dim_size[1] * localYLenght + x;
//        }
//    }
    //double test = ProcRank;
    MPI_Gatherv (localC, localXLenght * localYLenght, MPI_DOUBLE, C, receive_counts, receive_displacements, type4, 0, myDecComm);
    //MPI_Gatherv (&test, 1, MPI_DOUBLE, C, receive_counts, receive_displacements, MPI_DOUBLE, 0, myDecComm);
    if (ProcRank == 0)
        cout << MPI_Wtime () - strtTime<<"time\n";
    if (ProcRank == 0)
    {
        //printMatrix (C, BxLenght, AyLenght);

        cout << C[0];

    }
    delete[] localA;
    delete[] localB;
    delete[] localC;
    delete[] receive_counts;
    delete[] receive_displacements;
}
	



