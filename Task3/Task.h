#pragma once
#define _USE_MATH_DEFINES
#define DBL_MAX 1.7976931348623158e+308
#define MAX_ITERS 1000000

#include<iostream>
#include<limits>
#include<cstdlib>
#include<mpi.h>


class Task
{
	int ProcCount, ProcRank;
	int AxLenght, AyLenght;
    int BxLenght, ByLenght;
    int coords[2];
    MPI_Comm myDecComm;
    MPI_Comm rowComm;
    MPI_Comm colComm;

	double* A;
    double* B;
    double* C;
    double* localA;
    double* localB;
    double* localC;
    int localXLenght;
	int localYLenght;
    void multiply();
    void printMatrix(double* m,int mx, int my);

	//void set();
public:
	void run();
    Task(char* file_name);
	~Task();
};