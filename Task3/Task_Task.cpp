#include <fstream>
#include <time.h>
#include "Task.h"

using namespace std;

void setMatrix (int y1Lenght, int x1Lenght, int y2Lenght, int x2Lenght)
{
    srand((unsigned int) time(0));
    ofstream f;
    f.open ("in2.txt");
    f << y1Lenght << " " << x1Lenght << endl;
    for (int y = 0; y < y1Lenght; ++y)
    {
        for (int x = 0; x < x1Lenght; x++)
        {
            f << rand () % 10 << " ";
        }
        f << endl;
    }
    f << y2Lenght << " " << x2Lenght << endl;
    for (int y = 0; y < y2Lenght; ++y)
    {
        for (int x = 0; x < x2Lenght; x++)
        {
            f << rand () % 10 << " ";
        }
        f << endl;
    }
    f.close ();

}
Task::Task (char *file_name)
{
    MPI_Comm_size (MPI_COMM_WORLD, &ProcCount);
    MPI_Comm_rank (MPI_COMM_WORLD, &ProcRank);
    if (ProcRank == 0)
    {
        setMatrix (1000,1000,1000,1000);
        ifstream fin;
        fin.open (file_name, ios_base::in);

        if (fin.is_open ())
        {
            fin >> AyLenght;
            fin >> AxLenght;
            A = new double[AxLenght * AyLenght];
            //cout << AxLenght << " " << AyLenght << endl;
            for (int y = 0; y < AyLenght; y++)
                for (int x = 0; x < AxLenght; x++)
                {
                    fin >> A[y * AxLenght + x];
                }
            fin >> ByLenght;
            fin >> BxLenght;
            if (AxLenght != ByLenght)
            {
                cout << "AxLenght != ByLenght";
                exit (2);
            }
            B = new double[BxLenght * ByLenght];
            //cout << BxLenght << " " << ByLenght << endl;
            for (int y = 0; y < ByLenght; y++)
                for (int x = 0; x < BxLenght; x++)
                {
                    fin >> B[y * BxLenght + x];
                }
            fin.close ();
        }
        else
        {
            cout << "file open error";
            exit (1);
        }
    }
}

Task::~Task ()
{
    if (ProcRank == 0)
    {
        delete[] A;
        delete[] B;
        delete[] C;
    }
}