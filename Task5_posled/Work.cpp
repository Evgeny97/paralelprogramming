//
// Created by root on 18.05.17.
//

#include <cmath>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include "Work.h"


using namespace std;


int Work::Start()
{
    double all_time = MPI_Wtime();

    ifstream fin;
    fin.open("tasks.txt");
    int garbage = 0;

    double ts;
    int iter = load_new_tasks(fin);
    while (iter)
    {
        for (int j = 0; j < iter; ++j)
        {
            int maxi = taskStorage.back();
            taskStorage.pop_back();
            ts = MPI_Wtime();
            for (int i = 0; i < maxi; ++i)
                garbage += sqrt(i);
            work_done += maxi;
            time_of_counting += (MPI_Wtime() - ts);
        }
        iter = load_new_tasks(fin);
    }
    all_time = MPI_Wtime() - all_time;

    cout <<" work done: " << work_done << ", count time: " << time_of_counting
         << ", all time: " << all_time << endl;
    return garbage;
}

//int Work::put_task()
//{
////    int request = 10;
////    MPI_Send(&request, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
////    int taskRank = 0;
////    MPI_Recv(&taskRank, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, nullptr);
////    pthread_mutex_lock(&taskStorageMutex);
////    taskStorage.push_back(taskRank);
////    pthread_mutex_unlock(&taskStorageMutex);
////    return 0;
//}



int Work::load_new_tasks(ifstream &fin)
{
    int number_of_tasks;
    fin >> number_of_tasks;
    cout << "task_generator number  of tasks " << number_of_tasks << endl;
    for (int i = 0; i < number_of_tasks; ++i)
    {
        int q;
        fin >> q;
        taskStorage.push_back(q);
    }
    return number_of_tasks;
}







