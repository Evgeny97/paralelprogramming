//
// Created by root on 18.05.17.
//

#ifndef TASK5_WORK_H
#define TASK5_WORK_H

#include "mpi.h"

#include <vector>


using namespace std;

class Work
{
protected:
    long work_done =0;
    double time_of_counting;
    int load_new_tasks(ifstream &fin);
    vector<int> taskStorage;
public:
    int Start();
};
#endif //TASK5_WORK_H
