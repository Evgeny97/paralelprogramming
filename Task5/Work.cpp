//
// Created by root on 18.05.17.
//

#include <cmath>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include "Work.h"
#include "TaskThread.h"

using namespace std;

Work::Work()
{
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);    /* get current process id */
    MPI_Comm_size(MPI_COMM_WORLD, &ProcSize);    /* get number of processes */
    //pthread_mutex_init(&waitingMutex, NULL);
    pthread_mutex_init(&taskStorageMutex, NULL);
    pthread_cond_init(&waitingCondition, NULL);
}

int Work::Start()
{
    double all_time = MPI_Wtime();
    if (ProcRank != 0)
        task_supplyer();
    else
        task_generator();
    all_time = MPI_Wtime() - all_time;
    MPI_Barrier(MPI_COMM_WORLD);
    for (int l = 0; l < ProcSize; ++l)
    {
        if (ProcRank == l)
            cout << "Proc № " << ProcRank << ", work done: " << work_done << ", count time: " << time_of_counting
                 << ", all time: " << all_time << endl;
        MPI_Barrier(MPI_COMM_WORLD);
    }
    return 0;
}

//int Work::put_task()
//{
////    int request = 10;
////    MPI_Send(&request, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
////    int taskRank = 0;
////    MPI_Recv(&taskRank, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, nullptr);
////    pthread_mutex_lock(&taskStorageMutex);
////    taskStorage.push_back(taskRank);
////    pthread_mutex_unlock(&taskStorageMutex);
////    return 0;
//}

int Work::get_task()
{
    struct timespec ts;
    struct timeval tp;
    int taskRank = 0;
    pthread_mutex_lock(&taskStorageMutex);
    //cout << "task_supplyer taskStorage.size() "<< taskStorage.size()<<" in mpi proc № " << ProcRank << endl;
    while (taskStorage.size() == 0)
    {
        gettimeofday(&tp, NULL);
        ts.tv_sec = tp.tv_sec;
        ts.tv_nsec = tp.tv_usec * 1000 + 100;

        pthread_cond_timedwait(&waitingCondition, &taskStorageMutex, &ts);
        //pthread_cond_wait(&waitingCondition, &taskStorageMutex);
        if (!runing)
            break;
    }
    if (taskStorage.size() != 0)
    {
        taskRank = taskStorage.back();
        taskStorage.pop_back();
    }
    pthread_cond_signal(&waitingCondition);
    pthread_mutex_unlock(&taskStorageMutex);
    return taskRank;
}

void Work::task_supplyer()
{
    cout << "task_supplyer start in mpi proc № " << ProcRank << endl;
    pthread_mutex_lock(&taskStorageMutex);
    TaskThread executingThread(this);
    executingThread.start();
    MPI_Status status;
    while (runing)
    {
        if (taskStorage.size() == 0)
        {
            int request = 10;
            MPI_Send(&request, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
            int taskRank = 0;
            MPI_Recv(&taskRank, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
            if (taskRank == 0)
                runing = false;
            else
                taskStorage.push_back(taskRank);
            pthread_cond_signal(&waitingCondition);
        } else
            pthread_cond_wait(&waitingCondition, &taskStorageMutex);
    }
    pthread_mutex_unlock(&taskStorageMutex);
    executingThread.wait();
    cout << "task_supplyer stop in mpi proc № " << ProcRank << endl;
}

void Work::task_generator()
{

    cout << "task_generator start in mpi proc № " << ProcRank << endl;
    MPI_Status status;
    TaskThread executingThread(this);
    ifstream fin;
    fin.open("tasks.txt");
    load_new_tasks(fin);
    executingThread.start();

    while (runing)
    {
        int request = 10;
        MPI_Recv(&request, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);

        int taskRank = 0;
        if (taskStorage.size() != 0)
        {
            pthread_mutex_lock(&taskStorageMutex);
            taskRank = taskStorage.back();
            taskStorage.pop_back();
            pthread_cond_signal(&waitingCondition);
            pthread_mutex_unlock(&taskStorageMutex);
            MPI_Send(&taskRank, 1, MPI_INT, status.MPI_SOURCE, 1, MPI_COMM_WORLD);

        } else
        {
            for (int i = 0; i < ProcSize - 2; ++i)
            {
                MPI_Recv(&request, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
            }
            pthread_mutex_lock(&taskStorageMutex);
            if (load_new_tasks(fin))
            {
                //условие что задач больше чем процессов было в задании!!!
                for (int i = 0; i < ProcSize - 1; ++i)
                {
                    taskRank = taskStorage.back();
                    taskStorage.pop_back();
                    MPI_Send(&taskRank, 1, MPI_INT, i + 1, 1, MPI_COMM_WORLD);
                }
            } else
            {
                runing = false;
                for (int i = 0; i < ProcSize - 1; ++i)
                {
                    taskRank = 0;
                    MPI_Send(&taskRank, 1, MPI_INT, i + 1, 1, MPI_COMM_WORLD);
                }
            }
            pthread_cond_signal(&waitingCondition);
            pthread_mutex_unlock(&taskStorageMutex);
        }
    }

    fin.close();
    executingThread.wait();
    cout << "task_generator stop in mpi proc № " << ProcRank << endl;
}

int Work::task_executor()
{
    cout << "task_exec start in mpi proc № " << ProcRank << endl;
    int garbage = 0;
    double ts;
    while (runing)
    {
        int maxi = get_task();
//        if(!runing)
//            break;
        ts = MPI_Wtime();
        for (int i = 0; i < maxi; ++i)
            garbage += sqrt(i);
        work_done += maxi;
        time_of_counting += (MPI_Wtime() - ts);
        //cout << "task_exec count task:" << maxi << "in mpi proc № " << ProcRank << endl;
    }
    cout << "task_exec stop in mpi proc № " << ProcRank << endl; //<< " work_done: " << work_done ;
    return garbage;
}

int Work::load_new_tasks(ifstream &fin)
{
    int number_of_tasks;
    fin >> number_of_tasks;
    cout << "task_generator number  of tasks " << number_of_tasks << endl;
    for (int i = 0; i < number_of_tasks; ++i)
    {
        int q;
        fin >> q;
        taskStorage.push_back(q);
    }
    return number_of_tasks;
}

Work::~Work()
{
    pthread_mutex_destroy(&taskStorageMutex);
    //pthread_mutex_destroy(&waitingMutex);
    pthread_cond_destroy(&waitingCondition);
}






