//
// Created by root on 17.05.17.
//

#include <iostream>
#include "TaskThread.h"

using namespace std;

int TaskThread::start()
{
    return pthread_create(&threadId, NULL, TaskThread::thread_func, work);
}

int TaskThread::wait()
{
    return pthread_join(threadId, NULL);
}

void *TaskThread::thread_func(void *d)
{
    (static_cast< Work * >( d ))->task_executor();
    return nullptr;
}

TaskThread::TaskThread(Work *w)
{
    work = w;
}