//
// Created by root on 18.05.17.
//

#ifndef TASK5_WORK_H
#define TASK5_WORK_H

#include "mpi.h"

#include <vector>


using namespace std;

class Work
{
protected:

    int ProcRank, ProcSize;
    bool runing = true;
    long work_done =0;
    double time_of_counting;
    pthread_mutex_t taskStorageMutex;
    //pthread_mutex_t waitingMutex;
    pthread_cond_t waitingCondition;
    vector<int> taskStorage;
    //int put_task();

    int get_task();


public:
    Work();

    ~Work();

    int Start();

    void task_supplyer();
    int load_new_tasks(ifstream &fin);

    void task_generator();

    int task_executor();

};
#endif //TASK5_WORK_H
