//
// Created by root on 17.05.17.
//

//#ifndef TASK5_TASKTHREAD_H
//#define TASK5_TASKTHREAD_H

#include "Work.h"

class TaskThread
{
public:
    TaskThread(Work *w);

    int start();

    int wait();

protected:
    Work *work;

    static void *thread_func(void *d);

    pthread_t threadId;
};

//#endif //TASK5_TASKTHREAD_H
