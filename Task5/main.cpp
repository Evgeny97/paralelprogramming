#include <iostream>
#include <mpi.h>
#include <fstream>
#include "Work.h"
#include <cstdlib>


using namespace std;

void setTasks()
{
    ofstream fout;
    fout.open("tasks.txt");
    srand((unsigned int) time(0));
    for (int j = 0; j < 15; ++j)
    {
        fout << 100 << endl;
        for (int i = 0; i < 100; ++i)
        {
            fout << 1 + rand()%10000000 << endl;
        }
    }
    fout << 0 << endl;
}

int main(int argc, char **argv)
{
    //setTasks();
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

    if (provided != MPI_THREAD_MULTIPLE)
    {
        cout << "Can't init mpi in MPI_THREAD_MULTIPLE provided = " << provided;
        //MPI_Init(&argc, &argv);
    }

    Work w;
    w.Start();

    MPI_Finalize();
    return 0;
}



