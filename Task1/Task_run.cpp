#include"Task.h"

using namespace std;

void Task::run (int way)
{
    const double epsilon = 0.00001;
    int *sRcounts = new int[ProcCount];
    int *sRdis = new int[ProcCount];
    double *localA = new double[N * localN];
    double *localb = NULL;
    double *localx = new double[localN];
    double lastMeasure = DBL_MAX;
    double newMeasure;
//    clock_t workTime = 0;
    double workTime;
    int iters = 0;

    set (way);
    MPI_Bcast (&measureOfB, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (way > 2)
        localb = new double[localN];

    int curDis = 0;
    if (ProcRank == 0)
    {
        for (int i = 0; i < ProcCount; i++)
        {
            sRcounts[i] = N * ((N / ProcCount) + ((N % ProcCount) > i));
            sRdis[i] = curDis;
            curDis += sRcounts[i];
        }
//        workTime = clock ();
        workTime = MPI_Wtime ();
    }

    MPI_Scatterv (A, sRcounts, sRdis, MPI_DOUBLE, localA, N * localN, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    //MPI_Bcast(&measureOfB, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    curDis = 0;
    for (int i = 0; i < ProcCount; i++)
    {
        sRcounts[i] = (N / ProcCount) + ((N % ProcCount) > i);
        sRdis[i] = curDis;
        curDis += sRcounts[i];
    }

    if (way <= 2)
        MPI_Bcast (b, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    else
        MPI_Scatterv (b, sRcounts, sRdis, MPI_DOUBLE, localb, localN, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    while (lastMeasure > epsilon && iters < MAX_ITERS)
    {
        iters++;
        double localSumSquads = 0;
        double allSumSquads = 0;
        if (way <= 2)
        {
            multiplyLAXLX (localA, localx);
//            cout << ProcRank << ": ";
//            printX(localx, localN);
            for (int i = sRdis[ProcRank]; i < sRdis[ProcRank] + localN; i++)
            {
                localx[i - sRdis[ProcRank]] -= b[i];
                localSumSquads += localx[i - sRdis[ProcRank]] * localx[i - sRdis[ProcRank]];
            }
        }
        else
        {
            multiplyLAXLXpart_by_part (localA, localx, sRcounts, sRdis);
            /*cout << ProcRank << ": ";
            printX(localx, localN);		*/
            for (int i = 0; i < localN; i++)
            {
                localx[i] -= localb[i];
                localSumSquads += localx[i] * localx[i];
            }
        }
        //MPI_Gatherv(localx, localN, MPI_DOUBLE, AXminB, sRcounts, sRdis, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        //MPI_Allgather(&sumSquads, 1, MPI_DOUBLE, arrOfSquads, 1, MPI_DOUBLE, MPI_COMM_WORLD);
        MPI_Allreduce (&localSumSquads, &allSumSquads, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        if (way <= 2)
        {
            for (int i = sRdis[ProcRank]; i < sRdis[ProcRank] + localN; i++)
            {
                localx[i - sRdis[ProcRank]] = X[i] - (tau * (localx[i - sRdis[ProcRank]]));
            }
            MPI_Allgatherv (localx, localN, MPI_DOUBLE, X, sRcounts, sRdis, MPI_DOUBLE, MPI_COMM_WORLD);
        }
        else
        {
            for (int i = 0; i < localN; i++)
            {
                X[i] = X[i] - (tau * localx[i]);
            }
        }
        newMeasure = measureOfSums (allSumSquads) / measureOfB;
        //cout << newMeasure << endl;
        if (newMeasure > lastMeasure)
            tau = -tau;
        lastMeasure = newMeasure;
    }

    if (ProcRank == 0)
    {
        if (iters != MAX_ITERS)
        {
            //workTime = clock () - workTime;
            workTime = MPI_Wtime () - workTime;
            cout << "\nThe answer was found in " << iters << " iters\n";
            cout << "Measure of difference is: " << lastMeasure << "\n";
//            cout << "Work time: " << workTime << " clock ticks or " << (double) workTime / CLOCKS_PER_SEC << " sec\n";
            cout << "Work time: " << workTime << " sec\n";
        }
        else
        {
            cout << "Can't find answer in " << MAX_ITERS << " iters\n";
            cout << "Measure of difference is: " << lastMeasure << "\n";
        }
    }
    delete[] localx;
    delete[] localA;
    if (localb != NULL)
        delete[]localb;
}
	



