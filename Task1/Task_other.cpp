#include"Task.h"
#include<cmath>
#include <cstdio>


using namespace std;


void Task::printA()
{
	for (int _y = 0; _y < N; _y++)
	{
		for (int _x = 0; _x < N; _x++)
		{
			printf("%.2f ", A[_x + _y*N]);
		}
		cout << endl;
	}
}


void Task::printX(double* x, int n/* = this.N*/)
{
	for (int i = 0; i < n; i++)
	{
		printf("%.2f ", x[i]);
	}
	cout << endl;
}

void Task::printX(double* x)
{
	printX(x, N);
}

double Task::measure(double* x)
{
	double sum = 0;
	for (int i = 0; i < N; i++)
	{
		sum += x[i] * x[i];
	}
	return sqrt(sum);
}

double Task::measureOfSums(double x)
{
	return sqrt(x);
}

void Task::multiplyAvec(double* vec, double* rez)
{
	for (int _y = 0; _y < N; _y++)
	{
		double sum = 0;
		for (int _x = 0; _x < N; _x++)
		{
			sum += A[_x + _y*N] * vec[_x];
		}
		rez[_y] = sum;
	}
}

void Task::multiplyLAXLX(double* localA, double* localx)
{
	for (int _y = 0; _y < localN; _y++)
	{
		double sum = 0;
		for (int _x = 0; _x < N; _x++)
		{
			sum += localA[_x + _y*N] * X[_x];
		}
		localx[_y] = sum;
	}
}

void Task::multiplyLAXLXpart_by_part(double* localA, double* localx, int* sRcounts, int* sRdis)
{
	for (int _y = 0; _y < localN; _y++)
		localx[_y] = 0;

	for (int i = 0; i < ProcCount; i++)
	{
		for (int _y = 0; _y < localN; _y++)
		{
			double sum = 0;
			int q;
			int _x;
			for (_x = sRdis[(ProcCount + ProcRank - i) % ProcCount], q = 0; _x < sRdis[(ProcCount + ProcRank - i) % ProcCount] + sRcounts[(ProcCount + ProcRank - i) % ProcCount]; _x++, q++)
			{
				sum += localA[_x + _y*N] * X[q];
			}
			localx[_y] += sum;
			/*if (ProcRank == 0)
			{
				cout << ProcRank << "- rank, i= " << i << ", sum = " << sum << endl;
				cout << ProcRank << "- rank, sRdis[(ProcCount + ProcRank - i) % ProcCount]" << sRdis[(ProcCount + ProcRank - i) % ProcCount] << endl;
				cout << ProcRank << "- rank, sRcounts[(ProcCount + ProcRank - i) % ProcCount]" << sRcounts[(ProcCount + ProcRank - i) % ProcCount] << endl;
			}*/
		}
		/*MPI_Barrier(MPI_COMM_WORLD);
		if (ProcRank == 0)
		{
			cout << ProcRank << "- rank, i= " << i << endl;
			printX(X, (N / ProcCount) + 1);
		}
		MPI_Barrier(MPI_COMM_WORLD);
		if (ProcRank == 1)
		{
			cout << ProcRank << "- rank, i= " << i << endl;
			printX(X, (N / ProcCount) + 1);
		}
		MPI_Barrier(MPI_COMM_WORLD);*/
		MPI_Status status;
		MPI_Sendrecv_replace(X, (N / ProcCount) + 1, MPI_DOUBLE, (ProcRank + 1) % ProcCount, 1, (ProcRank + ProcCount - 1) % ProcCount, 1, MPI_COMM_WORLD, &status);
		/*if (ProcRank == 0)
		{
			cout << ProcRank << "- rank, i= " << i << endl;
			printX(X, (N / ProcCount) + 1);
		}*/

	}
}


void Task::set(int way)
{

	switch (way)
	{
	case 1:
	case 3:
		if (ProcRank == 0)
		{
			for (int _y = 0; _y < N; _y++)
			{
				for (int _x = 0; _x < N; _x++)
				{
					if (_x == _y)
						A[_x + _y*N] = 2.0f;
					else
						A[_x + _y*N] = 1.0f;
				}

				b[_y] = N + 1;
			}
			//printA();
			measureOfB = measure(b);
		}
		if (way == 1)
			for (int _y = 0; _y < N; _y++)
				X[_y] = 0;
		else
			for (int _y = 0; _y < (N / ProcCount) + 1; _y++)
				X[_y] = 0;

		break;
	case 2:
	case 4:
		if (ProcRank == 0)
		{
			double* tArr = new double[N];
			for (int _y = 0; _y < N; _y++)
			{
				for (int _x = 0; _x < N; _x++)
				{
					if (_x == _y)
						A[_x + _y*N] = 2.0f;
					else
						A[_x + _y*N] = 1.0f;
				}
				tArr[_y] = sin(2 * M_PI*_y / N);				
			}
			//printX(tArr);
			multiplyAvec(tArr, b);
			//printA();
			measureOfB = measure(b);
			delete[] tArr;
		}
		if (way == 2)
			for (int _y = 0; _y < N; _y++)
				X[_y] = 0;
		else
			for (int _y = 0; _y < (N / ProcCount) + 1; _y++)
				X[_y] = 0;

		break;
	default:
		break;

	}
}

Task::Task(int way, int n)
{
	N = n;
	/* Узнаем количество задач в запущенном приложении */
	MPI_Comm_size(MPI_COMM_WORLD, &ProcCount);
	/* ... и свой собственный номер: от 0 до (size-1) */
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
	localN = (N / ProcCount) + ((N%ProcCount) > ProcRank);
	if (ProcRank == 0)
	{
		A = new double[N*N];
	}
	if (way <= 2)
	{
		b = new double[N];
		X = new double[N];
	}
	else
	{
		if (ProcRank == 0)
			b = new double[N];
		X = new double[(N / ProcCount) + 1];
	}
}


Task::~Task()
{
	if (ProcRank == 0)
	{
		delete[] A;
	}
	delete[] X;
	if (b != NULL)
		delete[] b;
}
