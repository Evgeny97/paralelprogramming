#include"Task.h"

using namespace std;

void Task::run (int way)
{
    const double epsilon = 0.00001;
    double lastMeasure = DBL_MAX;
    double newMeasure;
    double *X1 = new double[N];

//    clock_t workTime = 0;
    double workTime;
    int iters = 0;
    set (way);
    for (int i = 0; i < N; i++)
        X1[i] = 0;
    workTime = MPI_Wtime ();

    while (lastMeasure > epsilon && iters < MAX_ITERS)
    {
        iters++;
        double localSumSquads = 0;
        multiplyAvec (X, X1);
        for (int i = 0; i < N; i++)
        {
            X1[i] -= b[i];
            localSumSquads += X1[i] * X1[i];
        }

        for (int i = 0; i < N; i++)
        {
            X1[i] = X[i] - (tau * (X1[i]));
        }
        //printX(X1);
        swap<double *> (X, X1);
        newMeasure = measureOfSums (localSumSquads) / measureOfB;
        //cout << newMeasure << endl;
        if (newMeasure > lastMeasure)
            tau = -tau;
        lastMeasure = newMeasure;
    }

    if (iters != MAX_ITERS)
    {
        //workTime = clock () - workTime;
        workTime = MPI_Wtime () - workTime;
        cout << "\nThe answer was found in " << iters << " iters\n";
        cout << "Measure of difference is: " << lastMeasure << "\n";
        cout << "Work time: " << workTime << " sec\n";
    }
    else
    {
        cout << "Can't find answer in " << MAX_ITERS << " iters\n";
        cout << "Measure of difference is: " << lastMeasure << "\n";
    }
}
	



