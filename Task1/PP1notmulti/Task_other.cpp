#include"Task.h"
#include<cmath>
#include <cstdio>

using namespace std;

void Task::printA ()
{
    for (int _y = 0; _y < N; _y++)
    {
        for (int _x = 0; _x < N; _x++)
        {
            printf ("%.2f ", A[_x + _y * N]);
        }
        cout << endl;
    }
}

void Task::printX (double *x, int n/* = this.N*/)
{
    for (int i = 0; i < n; i++)
    {
        printf ("%.2f ", x[i]);
    }
    cout << endl;
}

void Task::printX (double *x)
{
    printX (x, N);
}

double Task::measure (double *x)
{
    double sum = 0;
    for (int i = 0; i < N; i++)
    {
        sum += x[i] * x[i];
    }
    return sqrt (sum);
}

double Task::measureOfSums (double x)
{
    return sqrt (x);
}

void Task::multiplyAvec (double *vec, double *rez)
{
    for (int _y = 0; _y < N; _y++)
    {
        double sum = 0;
        for (int _x = 0; _x < N; _x++)
        {
            sum += A[_x + _y * N] * vec[_x];
        }
        rez[_y] = sum;
    }
}

void Task::set (int way)
{
    switch (way)
    {
        case 1:
        case 3:

            for (int _y = 0; _y < N; _y++)
            {
                for (int _x = 0; _x < N; _x++)
                {
                    if (_x == _y)
                        A[_x + _y * N] = 2.0f;
                    else
                        A[_x + _y * N] = 1.0f;
                }
                b[_y] = N + 1;
            }
            //printA();
            measureOfB = measure (b);
            for (int _y = 0; _y < N; _y++)
                X[_y] = 0;

            break;
        case 2:
        case 4:
            double *tArr = new double[N];
            for (int _y = 0; _y < N; _y++)
            {
                for (int _x = 0; _x < N; _x++)
                {
                    if (_x == _y)
                        A[_x + _y * N] = 2.0f;
                    else
                        A[_x + _y * N] = 1.0f;
                }
                tArr[_y] = sin (2 * M_PI * _y / N);
            }
            //printX(tArr);
            multiplyAvec (tArr, b);
            //printA();
            measureOfB = measure (b);
            delete[] tArr;

            for (int _y = 0; _y < N; _y++)
                X[_y] = 0;
            break;
    }
}

Task::Task (int way, int n)
{
    N = n;
    localN = (N / ProcCount) + ((N % ProcCount) > ProcRank);
    A = new double[N * N];
    b = new double[N];
    X = new double[N];
}

Task::~Task ()
{
    delete[] A;
    delete[] X;
    delete[] b;
}
