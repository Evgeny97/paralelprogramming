#include "Task.h"

int main(int argc, char **argv)
{
	if (argc == 3)
	{
		//Инициализируем библиотеку
		MPI_Init(&argc, &argv);

		Task t(atoi(argv[1]), atoi(argv[2]));
		t.run(atoi(argv[1]));

		//Все задачи завершают выполнение 
		MPI_Finalize();
	}
	else
	{
		std::cout << "use 2 arguments way and N" << std::endl;
	}

	return 0;
}
